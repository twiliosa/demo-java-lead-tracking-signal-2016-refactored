package com.twilio.signal.demo.stats;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityAREACODE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLSTATUS;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCARRIER;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLEAD;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLOCATION;
import com.twilio.signal.demo.util.BaseServlet;

public class TwilioLeadTrackingSignalDemoStatsServlet extends BaseServlet {
	
	String responseJSON = null;
	ArrayList<String> pieChartColors = new ArrayList<String>();

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoStatsServlet.class.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		pieChartColors.add("13699c");
		pieChartColors.add("7ecdd2");
		pieChartColors.add("81d8d0");
		pieChartColors.add("106271");
		pieChartColors.add("d3e2ef");

		String defaultColor = "d54339";

		String type = request.getParameter("type");

		if (type.equalsIgnoreCase("leadcount")) {
			// return total number of leads
			int leadcount = TwilioLeadTrackingSignalDemoEntityLEAD.getLeadCount();
			responseJSON = "{\"item\":[{\"value\":" + leadcount
					+ ",\"text\":\"Total Lead Count\"}]}";
		}

		else if (type.equalsIgnoreCase("carriers")) {

			Map<String, Integer> carrierInfo = TwilioLeadTrackingSignalDemoEntityCARRIER
					.getLookupCarrierStats();
			logger.info("count: " + carrierInfo.size());

			Iterator carrierInfoItr = carrierInfo.entrySet().iterator();
			String responseJSONBase = "{\"value\":\"";
			String responsePieJSON = "{\"item\":[";
			String comma = ",";
			int colorCount = 0;
			String colorId = null;

			String carrierName = null;
			String carrierCount = null;
			String responseJSONListBase = "[";
			String responseJSONList = null;

			while (carrierInfoItr.hasNext()) {

				if (responseJSONList != null)
					responseJSONList = responseJSONList + comma;
				else
					responseJSONList = responseJSONListBase;

				Map.Entry pair = (Map.Entry) carrierInfoItr.next();
				if (responseJSONList != null)
					responseJSONList = responseJSONList
							+ "{\"title\":{\"text\": \"" + pair.getValue()
							+ "\"},\"description\":\"" + pair.getKey() + "\"}";

				// if (pieChartColors.get(colorCount) != null) {
				// colorId = pieChartColors.get(colorCount);
				// colorCount++;
				// } else
				// colorId = defaultColor;
				//
				// Map.Entry pair = (Map.Entry) carrierInfoItr.next();
				// System.out.println(pair.getKey() + " = " + pair.getValue());
				// responsePieJSON = responsePieJSON + "{\"value\":\""
				// + pair.getValue() + "\",\"label\":\"" + pair.getKey()
				// + "\",\"color\":\"" + colorId + "\"}";
				// if (carrierInfoItr.hasNext())
				// responsePieJSON = responsePieJSON + comma;
				// logger.info("json: " + responseJSON);
			}
			responseJSON = responseJSONList + "]";
			logger.info("json LIST: " + responseJSON);

		}

		else if (type.equalsIgnoreCase("callprogress")) {

			Entity entity = TwilioLeadTrackingSignalDemoEntityCALLSTATUS.getSingleCallState("callstate");
			String initiatedCount = entity.getProperty("initiatedCount")
					.toString();
			String ringingCount = entity.getProperty("ringingCount").toString();
			String inprogressCount = entity.getProperty("inprogressCount")
					.toString();
			String completedCount = entity.getProperty("completedCount")
					.toString();

			String responseJsonBase = "{\"item\":[{\"value\":\"";
			String responseJsonFunnel = responseJsonBase + initiatedCount
					+ "\"" + ","
					+ "\"label\":\"Initiated Calls\"},{\"value\":\""
					+ ringingCount + "\"" + ","
					+ "\"label\":\"Ringing Calls\"},{\"value\":\""
					+ inprogressCount + "\"" + ","
					+ "\"label\":\"Answered/In-Progress Calls\"},{\"value\":\""
					+ completedCount + "\"" + ","
					+ "\"label\":\"Completed Calls\"}]}";

			responseJSON = responseJsonFunnel;
		}

		else if (type.equalsIgnoreCase("address")) {

			String responseJSONBaseLocationBase = "{\"points\":{\"point\":[";
			String responseJSONBaseLocationMap = null;
			String comma = ",";

			Map<String, String> addressLatLong = TwilioLeadTrackingSignalDemoEntityLOCATION
					.getPhoneNumberAddressStats();
			logger.info("count: " + addressLatLong.size());

			Iterator addressInfoItr = addressLatLong.entrySet().iterator();

			while (addressInfoItr.hasNext()) {

				if (responseJSONBaseLocationMap != null)
					responseJSONBaseLocationMap = responseJSONBaseLocationMap
							+ comma;
				else
					responseJSONBaseLocationMap = responseJSONBaseLocationBase;

				Map.Entry pair = (Map.Entry) addressInfoItr.next();
				if (responseJSONBaseLocationMap != null)
					responseJSONBaseLocationMap = responseJSONBaseLocationMap
							+ "{\"latitude\": \"" + pair.getKey()
							+ "\",\"longitude\":\"" + pair.getValue() + "\"}";

			}
			responseJSON = responseJSONBaseLocationMap + "]}}";

		}

		else if (type.equalsIgnoreCase("areacodes")) {

			Map<String, String> areaCodeStats = TwilioLeadTrackingSignalDemoEntityAREACODE
					.getPhoneNumberAreaCodeStats();
			logger.info("count: " + areaCodeStats.size());

			Iterator carrierInfoItr = areaCodeStats.entrySet().iterator();
			String comma = ",";

			String carrierName = null;
			String carrierCount = null;
			String responseJSONListBase = "[";
			String responseJSONList = null;

			while (carrierInfoItr.hasNext()) {

				if (responseJSONList != null)
					responseJSONList = responseJSONList + comma;
				else
					responseJSONList = responseJSONListBase;

				Map.Entry pair = (Map.Entry) carrierInfoItr.next();
				if (responseJSONList != null)
					responseJSONList = responseJSONList
							+ "{\"title\":{\"text\": \""
							+ pair.getValue().toString()
							+ "\"},\"description\":\" Area Code: " + " "
							+ pair.getKey().toString() + "\"}";

			}
			responseJSON = responseJSONList + "]";
		}

		else if (type.equalsIgnoreCase("experience")) {

			Map<String, String> experienceStats = TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE
					.getCallExperienceStats();
			logger.info("count: " + experienceStats.size());

			Iterator experienceStatsItr = experienceStats.entrySet().iterator();
			String comma = ",";

			String carrierName = null;
			String carrierCount = null;
			String responseJSONListBase = "[";
			String responseJSONList = null;

			while (experienceStatsItr.hasNext()) {

				if (responseJSONList != null)
					responseJSONList = responseJSONList + comma;
				else
					responseJSONList = responseJSONListBase;

				Map.Entry pair = (Map.Entry) experienceStatsItr.next();
				if (responseJSONList != null)
					responseJSONList = responseJSONList
							+ "{\"title\":{\"text\": \""
							+ pair.getValue().toString()
							+ "\"},\"description\":\" Experience Type: " + " "
							+ pair.getKey().toString() + "\"}";

			}
			responseJSON = responseJSONList + "]";
		}

		logger.info("json: " + responseJSON);
		response.getWriter().write(responseJSON);

	}

}
