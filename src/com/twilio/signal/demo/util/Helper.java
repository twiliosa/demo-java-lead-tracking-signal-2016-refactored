package com.twilio.signal.demo.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.datastore.Entity;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityAREACODE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLSTATUS;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCARRIER;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLOCATION;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityPHONENUMBER;

public class Helper {

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static String getUniqueHashCode(String categoryName) {
		return String.valueOf(categoryName.hashCode());
	}

	public static String getUniqueID() {
		return UUID.randomUUID().toString();
	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(this);
		} catch (IOException e) {
			return null;
		}
	}

	public static String getAreaCode(String phoneNumber, String countryCode) {

		String areaCode = null;

		if (countryCode == Constants.US_CC) {
			areaCode = phoneNumber.substring(2, 5);
		}

		return areaCode;

	}

	public static void resetTables() {

		Iterable<Entity> areaCodeItr = TwilioLeadTrackingSignalDemoEntityAREACODE
				.getAllareaCodes();
		Iterator<Entity> areaCodeIterator = areaCodeItr.iterator();
		while (areaCodeIterator.hasNext()) {
			Entity areaCodeEntity = areaCodeIterator.next();
			EntityUtil.deleteEntity(areaCodeEntity.getKey());
		}

		Iterable<Entity> callExpItr = TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE
				.getAllCallExperience();
		Iterator<Entity> callExpIterator = callExpItr.iterator();
		while (callExpIterator.hasNext()) {
			Entity callExpEntity = callExpIterator.next();
			EntityUtil.deleteEntity(callExpEntity.getKey());
		}

		Iterable<Entity> carrierItr = TwilioLeadTrackingSignalDemoEntityCARRIER
				.getAllCarriers();
		Iterator<Entity> carrierIterator = carrierItr.iterator();
		while (carrierIterator.hasNext()) {
			Entity carrierEntity = carrierIterator.next();
			EntityUtil.deleteEntity(carrierEntity.getKey());
		}

		Iterable<Entity> locationItr = TwilioLeadTrackingSignalDemoEntityLOCATION
				.getAllAreaCodeLocations();
		Iterator<Entity> locationIterator = locationItr.iterator();
		while (locationIterator.hasNext()) {
			Entity locationEntity = locationIterator.next();
			EntityUtil.deleteEntity(locationEntity.getKey());
		}

		Iterable<Entity> phoneNumberItr = TwilioLeadTrackingSignalDemoEntityPHONENUMBER
				.getAllPhoneNumbers();
		Iterator<Entity> phoneNumberIterator = phoneNumberItr.iterator();
		while (phoneNumberIterator.hasNext()) {
			Entity phoneNumberEntity = phoneNumberIterator.next();
			EntityUtil.deleteEntity(phoneNumberEntity.getKey());
		}

	}

	public static void resetCallStatus() {

		Iterable<Entity> callStatusItr = TwilioLeadTrackingSignalDemoEntityCALLSTATUS
				.getAllCallStates();
		Iterator<Entity> callStatusIterator = callStatusItr.iterator();
		while (callStatusIterator.hasNext()) {
			Entity callStatusEntity = callStatusIterator.next();
			EntityUtil.deleteEntity(callStatusEntity.getKey());
		}
	}

	public static boolean validatePhoneNumber(String phoneNo) {
		// validate phone numbers of format "1234567890"
		if (phoneNo.matches("\\d{10}"))
			return true;
		// validating phone number with -, . or spaces
		else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
			return true;
		// validating phone number with extension length from 3 to 5
		else if (phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}"))
			return true;
		// validating phone number where area code is in braces ()
		else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}"))
			return true;
		// return false if nothing matches the input
		else
			return false;

	}

}
