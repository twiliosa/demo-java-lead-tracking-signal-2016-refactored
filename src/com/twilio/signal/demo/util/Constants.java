package com.twilio.signal.demo.util;

public class Constants {
	
	/** SET and CHECK **/
	
	public static String ACCOUNT_SID = "ACac21b4749127eec9f471d41f87683855";
	public static String AUTH_TOKEN = "788fc4795e97c3d295ffdeabb8cce301";
	
	/** DEFAULTS **/
	public static String DEFAULT_PHONENUMBER = "+";
	public static String DEFAULT_CITY = "San Francisco";
	public static String US_CC = "+1";

	/** URLs **/
	public static String BASE_URL_LOCAL = "http://twilio-owlinsurance.com";
	public static String CALL_STATUS_EVENTS_URL = BASE_URL_LOCAL+"/callstatus";
	public static String WPP_URL = "https://proapi.whitepages.com/2.2/phone.json?api_key=3004c8acd188f32ffaa292fa95371582&phone_number=";
	public static String TWI_LOOKUP_WPP = "https://lookups.twilio.com/v1/PhoneNumbers/";
	public static String CALL_EXPERIENCE_URL = BASE_URL_LOCAL+"/callexperience?agent=";
	
	/** AGENTS **/
	public static int MAX_AGENTS = 0;
	public static String AGENT1_PHONENUMBER = "+";
	public static String AGENT2_PHONENUMBER = "+";
//	public static String AGENT3_PHONENUMBER = "";
//	public static String AGENT4_PHONENUMBER = "";
//	public static String AGENT5_PHONENUMBER = "";
	
	/** LABELS **/
	public static String CALL_EXPERIENCE_AGENT = "agent";
	public static String CALL_EXPERIENCE_GREETING = "greeting";
	
	

}
