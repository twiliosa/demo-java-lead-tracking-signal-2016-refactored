package com.twilio.signal.demo.calls;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.twilio.signal.demo.util.BaseServlet;
import com.twilio.signal.demo.util.Helper;

public class TwilioLeadTrackingSignalDemoCallsServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoCallsServlet.class
					.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		Helper.resetCallStatus();

		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();

		// Create "From", "To" mapping pairs and then set the outbound calls
		Map<String, String> callListToFrom = (Map<String, String>) syncCache
				.get("callList");
		Map<String, String> agentCallListMap = (Map<String, String>) syncCache
				.get("agentCallList");
		// All the Keys are phone numbers.
		callLeads(callListToFrom, agentCallListMap);

	}

	public void callLeads(Map<String, String> leadPhoneNumberMap,
			Map<String, String> agentCallListMap) {
		logger.log(Level.INFO, "callLEADS count:: " + leadPhoneNumberMap.size());

		TwilioLeadTrackingSignalDemoCallsOperations.setOutboundCalls(
				leadPhoneNumberMap, agentCallListMap);
	}

}
