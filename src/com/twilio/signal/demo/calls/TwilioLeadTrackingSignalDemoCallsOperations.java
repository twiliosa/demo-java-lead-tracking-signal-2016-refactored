package com.twilio.signal.demo.calls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.appengine.api.datastore.Entity;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLEAD;
import com.twilio.signal.demo.util.Constants;
import com.twilio.signal.demo.util.Helper;

public class TwilioLeadTrackingSignalDemoCallsOperations {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoCallsOperations.class
					.getCanonicalName());

	public static void setOutboundCalls(Map<String, String> leadPhoneNumberMap,
			Map<String, String> agentCallListMap) {

		TwilioRestClient client = new TwilioRestClient(Constants.ACCOUNT_SID,
				Constants.AUTH_TOKEN);

		logger.log(Level.INFO, "callLEADS in HERE");

		// Build a filter for the CallList
		Iterator<Map.Entry<String, String>> iterator = leadPhoneNumberMap
				.entrySet().iterator();

		// connect first 5 people to agents
		int agentExpCounter = 1;
		String experienceURL;

		while (iterator.hasNext()) {

			Map.Entry<String, String> entry = iterator.next();
			logger.log(Level.INFO, entry.getKey());
			logger.log(Level.INFO, entry.getValue());

			String calleePhoneNumber = entry.getKey();

			Entity leadEntity = TwilioLeadTrackingSignalDemoEntityLEAD
					.getSinglelead(calleePhoneNumber);
			String calleeName = leadEntity.getProperty("name").toString();
			logger.log(Level.INFO, "CALLEE NAME" + calleeName);
			StringTokenizer st = new StringTokenizer(calleeName);
			calleeName = st.nextToken();

			if (agentExpCounter <= Constants.MAX_AGENTS) {
				String agentKey = "agent" + String.valueOf(agentExpCounter);
				experienceURL = Constants.CALL_EXPERIENCE_URL
						+ agentCallListMap.get(agentKey) + "&callee="
						+ calleeName;
				agentExpCounter++;
				logger.log(Level.INFO, experienceURL);

			} else
				experienceURL = Constants.CALL_EXPERIENCE_URL + "machine"
						+ "&callee=" + calleeName;

			// Set outbound phone calls
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("To", entry.getKey()));
			params.add(new BasicNameValuePair("From", entry.getValue()));
			params.add(new BasicNameValuePair("Url", experienceURL));
			params.add(new BasicNameValuePair("StatusCallback",
					Constants.CALL_STATUS_EVENTS_URL));
			params.add(new BasicNameValuePair("StatusCallbackMethod", "POST"));
			params.add(new BasicNameValuePair("StatusCallbackEvent",
					"initiated"));
			params.add(new BasicNameValuePair("StatusCallbackEvent", "ringing"));
			params.add(new BasicNameValuePair("StatusCallbackEvent", "answered"));
			params.add(new BasicNameValuePair("StatusCallbackEvent",
					"completed"));
			String areaCode = Helper.getAreaCode(entry.getKey(), "US");
			logger.log(Level.INFO, areaCode);

			CallFactory callFactory = client.getAccount().getCallFactory();
			Call call = null;
			try {
				call = callFactory.create(params);
				logger.log(Level.INFO, call.getSid());

			} catch (TwilioRestException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
