package com.twilio.signal.demo.calls;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLSTATUS;
import com.twilio.signal.demo.util.BaseServlet;
import com.twilio.signal.demo.util.EntityUtil;


public class TwilioLeadTrackingSignalDemoCallStatusServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoCallStatusServlet.class.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String callStatus = request.getParameter("CallStatus");
		String callSid = request.getParameter("CallSid");

		logger.log(Level.INFO, "call Status: " + callStatus);

		Entity callStateEntity = TwilioLeadTrackingSignalDemoEntityCALLSTATUS
				.getSingleCallState("callstate");
		if (callStateEntity == null) {
			TwilioLeadTrackingSignalDemoEntityCALLSTATUS.createOrUpdateCallStates(0, 0, 0, 0);
		}

		callStateEntity = TwilioLeadTrackingSignalDemoEntityCALLSTATUS.getSingleCallState("callstate");

		if (callStatus.equalsIgnoreCase("initiated")) {
			String initiated = callStateEntity.getProperty("initiatedCount")
					.toString();
			Integer initiatedInt = Integer.valueOf(initiated);
			initiatedInt++;
			callStateEntity.setProperty("initiatedCount",
					initiatedInt.toString());

		} else if (callStatus.equalsIgnoreCase("ringing")) {
			String initiated = callStateEntity.getProperty("initiatedCount")
					.toString();
			Integer initiatedInt = Integer.valueOf(initiated);
			Integer ringingInt = Integer.valueOf(callStateEntity.getProperty(
					"ringingCount").toString());
			
			if (initiatedInt == 1 || initiatedInt < 1) {
				initiatedInt = 0;
			}
			
			ringingInt++;
			callStateEntity.setProperty("initiatedCount",
					initiatedInt.toString());
			callStateEntity.setProperty("ringingCount", ringingInt.toString());

		} else if (callStatus.equalsIgnoreCase("in-progress")) {
			String inprogress = callStateEntity.getProperty("inprogressCount")
					.toString();
			Integer inprogressInt = Integer.valueOf(inprogress);
			Integer ringingInt = Integer.valueOf(callStateEntity.getProperty(
					"ringingCount").toString());
			inprogressInt++;
			ringingInt--;
			
			if (ringingInt == 1 || ringingInt < 1) {
				ringingInt = 0;
			}
			
			callStateEntity.setProperty("inprogressCount",
					inprogressInt.toString());
			callStateEntity.setProperty("ringingCount", ringingInt.toString());

		} else if (callStatus.equalsIgnoreCase("completed")) {
			String completed = callStateEntity.getProperty("completedCount")
					.toString();
			Integer completedInt = Integer.valueOf(completed);
			Integer inprogressInt = Integer.valueOf(callStateEntity
					.getProperty("inprogressCount").toString());
			completedInt++;
			if (inprogressInt != 0) 
				inprogressInt--;
			
			callStateEntity.setProperty("completedCount",
					completedInt.toString());
			callStateEntity.setProperty("inprogressCount",
					inprogressInt.toString());
			
			Integer initiatedCountInt = Integer.valueOf(callStateEntity.getProperty(
					"initiatedCount").toString());
			if (initiatedCountInt == 1 || initiatedCountInt < 1) {
				initiatedCountInt = 0;
				callStateEntity.setProperty("initiatedCount", initiatedCountInt);
			}
			Integer ringingCountInt = Integer.valueOf(callStateEntity.getProperty(
					"ringingCount").toString());
			if (ringingCountInt == 1 || ringingCountInt < 1) {
				ringingCountInt = 0;
				callStateEntity.setProperty("ringingCount", ringingCountInt);
			}
			

		}
		EntityUtil.persistEntity(callStateEntity);

	}

}
