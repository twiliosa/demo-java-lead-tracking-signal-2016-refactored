package com.twilio.signal.demo.lead;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLEAD;
import com.twilio.signal.demo.util.BaseServlet;
import com.twilio.signal.demo.util.Constants;
import com.twilio.signal.demo.util.EntityUtil;
import com.twilio.signal.demo.util.Helper;

public class TwilioLeadTrackingSignalDemoLeadServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoLeadServlet.class
					.getCanonicalName());

	/**
	 * Get the requested lead entities in JSON format
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doGet(req, resp);
		logger.log(Level.INFO, "Obtaining lead information");
		String searchFor = req.getParameter("q");
		PrintWriter out = resp.getWriter();
		Iterable<Entity> entities = null;
		if (searchFor == null || searchFor.equals("")) {
			entities = TwilioLeadTrackingSignalDemoEntityLEAD.getAllleads();
			out.println(EntityUtil.writeJSON(entities));
		} else {
			entities = TwilioLeadTrackingSignalDemoEntityLEAD
					.getlead(searchFor);
			out.println(EntityUtil.writeJSON(entities));
		}
		return;
	}

	/**
	 * Insert the new lead
	 */
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.log(Level.INFO, "Creating lead");
		String nameKey = req.getParameter("m-name");
		String phnno = req.getParameter("m-phonenumber");
		String selectedOption = req.getParameter("m-selectsport");
		String countryCode = req.getParameter("m-countrycode");
		logger.log(Level.INFO, "leadID: " + nameKey);

		// validate phone number, else reject
		
		phnno = phnno.replaceAll("-","");
		phnno = phnno.replaceAll("\\(","");				
		phnno = phnno.replaceAll("\\)","");				
		phnno = phnno.replaceAll("\\s","");
		phnno = phnno.trim();
		
		if (Helper.validatePhoneNumber(phnno)) {

			logger.log(Level.INFO, "VALID: " +phnno);

			TwilioLeadTrackingSignalDemoEntityLEAD.createOrUpdatelead(nameKey,
					phnno, selectedOption, countryCode);
		}

		else
			logger.log(Level.INFO, "INVALID: " +phnno);

	}

	/**
	 * Delete the lead
	 */
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String leadName = req.getParameter("id");
		logger.log(Level.INFO, "Deleting User {0}", leadName);
		Key key = KeyFactory.createKey("leadname", leadName);
		try {
			// CASCADE_ON_DELETE
			Iterable<Entity> entities = EntityUtil.listChildKeys("Order", key);
			final List<Key> orderkeys = new ArrayList<Key>();
			final List<Key> linekeys = new ArrayList<Key>();
			for (Entity e : entities) {
				orderkeys.add(e.getKey());
				Iterable<Entity> lines = EntityUtil.listEntities("LineItem",
						"orderID", String.valueOf(e.getKey().getId()));
				for (Entity en : lines) {
					linekeys.add(en.getKey());
				}
			}
			EntityUtil.deleteEntity(linekeys);
			EntityUtil.deleteEntity(orderkeys);
			EntityUtil.deleteEntity(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Redirect the call to doDelete or doPut method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action.equalsIgnoreCase("delete")) {
			doDelete(req, resp);
			return;
		} else if (action.equalsIgnoreCase("put")) {
			doPut(req, resp);
			return;
		}
	}

	public String processLead() {

		// lookup the city from the Signal database

		return Constants.DEFAULT_CITY;
	}

}
