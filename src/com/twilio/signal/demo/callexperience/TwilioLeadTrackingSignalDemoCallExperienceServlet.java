package com.twilio.signal.demo.callexperience;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE;
import com.twilio.signal.demo.util.BaseServlet;

public class TwilioLeadTrackingSignalDemoCallExperienceServlet extends
		BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoCallExperienceServlet.class
					.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String agent = request.getParameter("agent");
		String calleeName = request.getParameter("callee");

		TwiMLResponse twiml = new TwiMLResponse();

		if (agent.equalsIgnoreCase("machine")) {
			experienceGreeting(calleeName, twiml);
			TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE
					.createOrUpdateCallExperience("machine");
		} else {
			experienceAgent(calleeName, twiml);
			TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE
					.createOrUpdateCallExperience("human");

		}

		response.setContentType("application/xml");
		response.getWriter().print(twiml.toXML());
	}

	public void experienceGreeting(String calleeName, TwiMLResponse twiml) {

		logger.log(Level.INFO, "experienceGREETING:::");

		Say say = new Say("Hello" + calleeName
				+ "Welcome to Signal TWENTY SIXTEEN");
		Say say2ndLine = new Say(
				"Thank you for taking part in this demo. Enjoy the rest of the conference");

		try {
			twiml.append(say);
			twiml.append(say2ndLine);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void experienceAgent(String calleeName, TwiMLResponse twiml) {

		Say say = new Say("Hello:" + " " + calleeName);
		// Dial dial = new Dial("");
		try {
			twiml.append(say);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.INFO, "experienceAGENT:::");
	}

}
