package com.twilio.signal.demo.admin;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityAREACODE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCALLSTATUS;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCARRIER;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLEAD;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLOCATION;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityPHONENUMBER;
import com.twilio.signal.demo.util.BaseServlet;
import com.twilio.signal.demo.util.EntityUtil;

public class TwilioLeadTrackingSignalDemoAdminServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoAdminServlet.class
					.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String type = request.getParameter("type");

		if (type.equalsIgnoreCase("reset")) {
			Iterable<Entity> areaCodeItr = TwilioLeadTrackingSignalDemoEntityAREACODE
					.getAllareaCodes();
			Iterator<Entity> areaCodeIterator = areaCodeItr.iterator();
			while (areaCodeIterator.hasNext()) {
				Entity areaCodeEntity = areaCodeIterator.next();
				EntityUtil.deleteEntity(areaCodeEntity.getKey());
			}

			Iterable<Entity> leadItr = TwilioLeadTrackingSignalDemoEntityLEAD
					.getAllleads();
			Iterator<Entity> leadIterator = leadItr.iterator();
			while (leadIterator.hasNext()) {
				Entity leadEntity = leadIterator.next();
				EntityUtil.deleteEntity(leadEntity.getKey());
			}

			Iterable<Entity> callExpItr = TwilioLeadTrackingSignalDemoEntityCALLEXPERIENCE
					.getAllCallExperience();
			Iterator<Entity> callExpIterator = callExpItr.iterator();
			while (callExpIterator.hasNext()) {
				Entity callExpEntity = callExpIterator.next();
				EntityUtil.deleteEntity(callExpEntity.getKey());
			}

			Iterable<Entity> callStatusItr = TwilioLeadTrackingSignalDemoEntityCALLSTATUS
					.getAllCallStates();
			Iterator<Entity> callStatusIterator = callStatusItr.iterator();
			while (callStatusIterator.hasNext()) {
				Entity callStatusEntity = callStatusIterator.next();
				EntityUtil.deleteEntity(callStatusEntity.getKey());
			}

			Iterable<Entity> carrierItr = TwilioLeadTrackingSignalDemoEntityCARRIER
					.getAllCarriers();
			Iterator<Entity> carrierIterator = carrierItr.iterator();
			while (carrierIterator.hasNext()) {
				Entity carrierEntity = carrierIterator.next();
				EntityUtil.deleteEntity(carrierEntity.getKey());
			}

			Iterable<Entity> locationItr = TwilioLeadTrackingSignalDemoEntityLOCATION
					.getAllAreaCodeLocations();
			Iterator<Entity> locationIterator = locationItr.iterator();
			while (locationIterator.hasNext()) {
				Entity locationEntity = locationIterator.next();
				EntityUtil.deleteEntity(locationEntity.getKey());
			}

			Iterable<Entity> phoneNumberItr = TwilioLeadTrackingSignalDemoEntityPHONENUMBER
					.getAllPhoneNumbers();
			Iterator<Entity> phoneNumberIterator = phoneNumberItr.iterator();
			while (phoneNumberIterator.hasNext()) {
				Entity phoneNumberEntity = phoneNumberIterator.next();
				EntityUtil.deleteEntity(phoneNumberEntity.getKey());
			}

		}
	}
}
