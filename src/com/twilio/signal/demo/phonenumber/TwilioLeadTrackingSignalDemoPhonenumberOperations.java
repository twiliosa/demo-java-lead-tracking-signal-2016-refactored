package com.twilio.signal.demo.phonenumber;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.message.BasicNameValuePair;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.twilio.sdk.LookupsClient;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.AvailablePhoneNumber;
import com.twilio.sdk.resource.instance.IncomingPhoneNumber;
import com.twilio.sdk.resource.instance.lookups.PhoneNumber;
import com.twilio.sdk.resource.list.AvailablePhoneNumberList;
import com.twilio.sdk.resource.list.IncomingPhoneNumberList;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityAREACODE;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityCARRIER;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLOCATION;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityPHONENUMBER;
import com.twilio.signal.demo.util.Constants;
import com.twilio.signal.demo.util.EntityUtil;

public class TwilioLeadTrackingSignalDemoPhonenumberOperations {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoPhonenumberOperations.class
					.getCanonicalName());

	public static Map<String, String> createToFromPairs(
			ArrayList<String> leadPhoneNumbers) {

		Map<String, String> callListToFrom = new HashMap<String, String>();

		TwilioRestClient client = new TwilioRestClient(Constants.ACCOUNT_SID,
				Constants.AUTH_TOKEN);

		Iterator<String> alIterator = leadPhoneNumbers.iterator();
		// Loop over numbers and print out a property for each one.
		while (alIterator.hasNext()) {
			
			logger.log(Level.INFO, "FROM PN");

			// format the phone number before hand
			//String formattedPhoneNumber = 
			
			// Build a filter for the IncomingPhoneNumberList
			Map<String, String> params = new HashMap<String, String>();
			String toPhoneNumber = alIterator.next();
			params.put("PhoneNumber", getAreaCode(toPhoneNumber));

			IncomingPhoneNumberList numbersList = client.getAccount()
					.getIncomingPhoneNumbers(params);

			Iterator<IncomingPhoneNumber> numbersListItr = numbersList
					.iterator();
			String fromPhoneNumber = null;
			logger.log(Level.INFO, "FROM PN 1");

			if (numbersListItr.hasNext())
				fromPhoneNumber = numbersListItr.next().getPhoneNumber();

			if (fromPhoneNumber == null) {
				// provision a new phone number
				String areaCode = getAreaCode(toPhoneNumber);
				if (areaCode != null) {
					logger.log(Level.INFO, "AREA CODE CHECK " +areaCode);

					// POST to IncomingPhoneNumbers to obtain a new PN
					// Build a filter for the IncomingPhoneNumberList
//					params = new HashMap<String, String>();
//					params.put("PhoneNumber", areaCode);
//					IncomingPhoneNumberList numbers = client.getAccount()
//							.getIncomingPhoneNumbers(params);
					params = new HashMap<String, String>();
					params.put("AreaCode", areaCode);
				    
				    AvailablePhoneNumberList numbers = client.getAccount().getAvailablePhoneNumbers(params, "US", "Local");
				    List<AvailablePhoneNumber> list = numbers.getPageData();
				    
				    // Purchase the first number in the list.
				    List<NameValuePair> purchaseParams = new ArrayList<NameValuePair>();
				    purchaseParams.add(new BasicNameValuePair("PhoneNumber", list.get(0).getPhoneNumber()));
				    try {
						client.getAccount().getIncomingPhoneNumberFactory().create(purchaseParams);
						fromPhoneNumber = list.get(0).getPhoneNumber();
					} catch (TwilioRestException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						logger.log(Level.INFO, "AREA CODE CHECK: FFROM NUMBER " +fromPhoneNumber);
				}
				if (fromPhoneNumber == null)
					fromPhoneNumber = Constants.DEFAULT_PHONENUMBER;
				logger.log(Level.INFO, "FROM PN 2");

			}

			callListToFrom.put(toPhoneNumber, fromPhoneNumber);
			addAreaCode(toPhoneNumber);
			logger.log(Level.INFO, fromPhoneNumber);

		}
		return callListToFrom;
	}

	/**
	 * 
	 * @param toPhoneNumber
	 * @param fromPhoneNumber
	 */

	public static void addPhoneNumber(String toPhoneNumber,
			String fromPhoneNumber) {

		Entity phoneNumberToFromEntity = new Entity("phonenumber",
				toPhoneNumber);
		phoneNumberToFromEntity.setProperty("tophonenumber", toPhoneNumber);
		phoneNumberToFromEntity.setProperty("fromphonenumber", fromPhoneNumber);
		EntityUtil.persistEntity(phoneNumberToFromEntity);

	}

	public static String getAreaCode(String phoneNumber) {
		String areaCode = null;
		if (phoneNumber != null) {
			areaCode = phoneNumber.substring(0, 3);
		}
		return areaCode;
	}

	public static void addAreaCode(String phoneNumber) {

		String areaCode = phoneNumber.substring(0, 3);
		logger.log(Level.INFO, "areaCode" + areaCode);
		TwilioLeadTrackingSignalDemoEntityAREACODE
				.createOrUpdateareaCode(areaCode);
	}

	public static boolean lookupPhoneNumber(String lookupPhoneNumber) {

		boolean lookupSuccessful = false;

		try {
			LookupsClient client = new LookupsClient(Constants.ACCOUNT_SID,
					Constants.AUTH_TOKEN);

			PhoneNumber phoneNumber = client.getPhoneNumber(lookupPhoneNumber,
					true);

			String phoneNumberType = phoneNumber.getType().toString();
			String phoneNumberCarrier = phoneNumber.getCarrierName();
			String phoneNumberCountryCode = phoneNumber.getCountryCode();

			// Lookup the phone number against Whitepages pro, create a
			// different datastore for it

			lookupSuccessful = true;

			TwilioLeadTrackingSignalDemoEntityPHONENUMBER
					.createOrUpdatePhoneNumber(lookupPhoneNumber,
							phoneNumberType, phoneNumberCarrier,
							phoneNumberCountryCode);
			TwilioLeadTrackingSignalDemoEntityCARRIER
					.createOrUpdatePhoneNumberCarrier(phoneNumberCarrier);

			//getPhoneNumberLookupWPP(lookupPhoneNumber);
			getPhoneNumberLookupWPPTwilioAddOn(lookupPhoneNumber);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lookupSuccessful;
	}
	
	public static void getPhoneNumberLookupWPPTwilioAddOn(String phoneNumber) {
		
		URLFetchService service = URLFetchServiceFactory.getURLFetchService();

		try {

			HTTPRequest fetchReq = new HTTPRequest(new URL(Constants.TWI_LOOKUP_WPP
					+ phoneNumber+"/?AddOns=whitepages_pro_caller_id"));
			String nameAndPassword = Constants.ACCOUNT_SID+":"+Constants.AUTH_TOKEN;
			String authorizationString = "Basic " + Base64.encodeBase64String(nameAndPassword.getBytes());
			
			fetchReq.addHeader(new HTTPHeader("Authorization", authorizationString));
			HTTPResponse fetchRes = service.fetch(fetchReq);
			
			logger.log(Level.INFO, "URL " + new URL(Constants.TWI_LOOKUP_WPP
					+ phoneNumber+"/?AddOns=whitepages_pro_caller_id").toString());
			
			

			if (fetchRes.getResponseCode() == 200) {
				
				JsonElement jelement = new JsonParser().parse(new String(
						fetchRes.getContent()));
				JsonObject jObject = jelement.getAsJsonObject(); // full object
				logger.log(Level.INFO, "jObject " + jObject.toString());				
				JsonObject jObjectAddOn = jObject.getAsJsonObject("add_ons");
				JsonObject jObjectResults = jObjectAddOn.getAsJsonObject("results");
				JsonObject jObjectWhitepagesProCallerId = jObjectResults.getAsJsonObject("whitepages_pro_caller_id");
				
				JsonObject jObjectResult = jObjectWhitepagesProCallerId.getAsJsonObject("result");
				logger.log(Level.INFO, "Result " + jObjectResult.toString());
				JsonArray jArray = jObjectResult.getAsJsonArray("results");
				JsonElement jElementResultsIn = jArray.get(0);
				JsonObject jObjectResultsIn = jElementResultsIn.getAsJsonObject();
				logger.log(Level.INFO, "ResultsIn " + jObjectResultsIn.toString());

				JsonObject jObjectAssLoc = jObjectResultsIn.getAsJsonObject();
				JsonArray jArrayAssLoc = jObjectAssLoc.getAsJsonArray("associated_locations");
				JsonElement jElementArrayAssLoc = jArrayAssLoc.get(0);
				JsonObject jObjectArrayAssLoc = jElementArrayAssLoc.getAsJsonObject();
				logger.log(Level.INFO, "jObjectArrayAssLoc " + jObjectArrayAssLoc.toString());
				
				JsonElement jElementLocation = jObjectArrayAssLoc
						.getAsJsonObject("lat_long");
				JsonObject jsonObjectLatLong = jElementLocation
						.getAsJsonObject();				
			

				String latitude = jsonObjectLatLong.get("latitude").toString();
				String longitude = jsonObjectLatLong.get("longitude")
						.toString();
				logger.log(Level.INFO, "latitude new: " + latitude);
				logger.log(Level.INFO, "longitude new: " + longitude);
				
				// add to the datastore
				TwilioLeadTrackingSignalDemoEntityLOCATION
						.createOrUpdatePhoneNumberLocation(phoneNumber,
								latitude, longitude);
			}
			else
				logger.log(Level.INFO, "fetch reesponse: " +fetchRes.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void getPhoneNumberLookupWPP(String phoneNumber) {

		URLFetchService service = URLFetchServiceFactory.getURLFetchService();

		try {

			HTTPRequest fetchReq = new HTTPRequest(new URL(Constants.WPP_URL
					+ phoneNumber));
			HTTPResponse fetchRes = service.fetch(fetchReq);
			if (fetchRes.getResponseCode() == 200) {

				JsonElement jelement = new JsonParser().parse(new String(
						fetchRes.getContent()));
				JsonObject jObject = jelement.getAsJsonObject();
				JsonArray jArray = jObject.getAsJsonArray("results");

				JsonElement jElementResult = jArray.get(0);
				JsonObject jObjectResult = jElementResult.getAsJsonObject();
				JsonElement jElementLocation = jObjectResult
						.getAsJsonObject("best_location");
				JsonObject jsonObjectLocation = jElementLocation
						.getAsJsonObject();

				JsonElement jsonElementLatLong = jsonObjectLocation
						.getAsJsonObject("lat_long");
				JsonObject jsonObjectLatLong = jsonElementLatLong
						.getAsJsonObject();

				String latitude = jsonObjectLatLong.get("latitude").toString();
				String longitude = jsonObjectLatLong.get("longitude")
						.toString();

				// String city = jsonObjectLocation.get("address").toString();
				// String postalCode = jsonObjectLocation.get("postal_code")
				// .toString();

				logger.log(Level.INFO, "latitude: " + latitude);
				logger.log(Level.INFO, "longitude: " + longitude);

				// add to the datastore
				TwilioLeadTrackingSignalDemoEntityLOCATION
						.createOrUpdatePhoneNumberLocation(phoneNumber,
								latitude, longitude);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
