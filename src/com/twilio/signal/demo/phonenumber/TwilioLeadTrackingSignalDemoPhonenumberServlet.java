package com.twilio.signal.demo.phonenumber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.twilio.signal.demo.calls.TwilioLeadTrackingSignalDemoCallsOperations;
import com.twilio.signal.demo.calls.TwilioLeadTrackingSignalDemoCallsServlet;
import com.twilio.signal.demo.entities.TwilioLeadTrackingSignalDemoEntityLEAD;
import com.twilio.signal.demo.util.BaseServlet;
import com.twilio.signal.demo.util.Constants;
import com.twilio.signal.demo.util.Helper;

public class TwilioLeadTrackingSignalDemoPhonenumberServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoCallsServlet.class
					.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		// reset the tables just in case we run this multiple times. Its stateless in nature
		
		
		Helper.resetTables();
		

		ArrayList<String> leadPhoneNumberAl = getLeadPhoneNumbers();

		// Create "From", "To" mapping pairs and then set the outbound calls
		Map<String, String> callListToFrom = TwilioLeadTrackingSignalDemoPhonenumberOperations
				.createToFromPairs(leadPhoneNumberAl);
		Map<String, String> agentCallListMap = prepareAgentCallList();
		runPhoneNumberLookup(leadPhoneNumberAl);

		// set these Maps for use later in CallsServlet
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers
				.getConsistentLogAndContinue(Level.INFO));
		String keyCallList = "callList";
		String keyAgentCalLList = "agentCallList";

		syncCache.put(keyCallList, callListToFrom);
		syncCache.put(keyAgentCalLList, agentCallListMap);

	}

	public ArrayList<String> getLeadPhoneNumbers() {

		ArrayList<String> leadPhoneNumberAl = new ArrayList<String>();
		Iterable<Entity> leads = TwilioLeadTrackingSignalDemoEntityLEAD
				.getAllleads();
		Iterator<Entity> leadItr = leads.iterator();

		while (leadItr.hasNext()) {
			Entity lead = leadItr.next();
			String phoneNumber = lead.getProperty("phnno").toString();
			leadPhoneNumberAl.add(phoneNumber);
		}

		return leadPhoneNumberAl;

	}

	public void callLeads(Map<String, String> leadPhoneNumberMap,
			Map<String, String> agentCallListMap) {
		logger.log(Level.INFO, "callLEADS");

		TwilioLeadTrackingSignalDemoCallsOperations.setOutboundCalls(
				leadPhoneNumberMap, agentCallListMap);
	}

	public void runPhoneNumberLookup(ArrayList<String> leadPhoneNumbersAl) {

		// run the lookup against all lead phone numbers and then pipe the data
		// to the stream
		logger.log(Level.INFO, String.valueOf(leadPhoneNumbersAl.size()));

		Iterator<String> leadPhoneNumberItr = leadPhoneNumbersAl.iterator();
		while (leadPhoneNumberItr.hasNext()) {
			TwilioLeadTrackingSignalDemoPhonenumberOperations
					.lookupPhoneNumber(leadPhoneNumberItr.next());
		}

	}

	public Map<String, String> prepareAgentCallList() {

		// load up agent phone numbers in memory
		Map<String, String> agentCallList = new HashMap<String, String>();
		agentCallList.put("agent1", Constants.AGENT1_PHONENUMBER);
		agentCallList.put("agent2", Constants.AGENT2_PHONENUMBER);
//		agentCallList.put("agent3", Constants.AGENT3_PHONENUMBER);
//		agentCallList.put("agent4", Constants.AGENT4_PHONENUMBER);
//		agentCallList.put("agent5", Constants.AGENT5_PHONENUMBER);

		return agentCallList;

	}

}
