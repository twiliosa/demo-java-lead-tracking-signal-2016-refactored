package com.twilio.signal.demo.entities;

import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.twilio.signal.demo.util.EntityUtil;

public class TwilioLeadTrackingSignalDemoEntityPHONENUMBER {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoEntityPHONENUMBER.class.getCanonicalName());

	public static void createOrUpdatePhoneNumber(String phonenumber,
			String phonenumber_type, String phonenumber_carrier,
			String phonenumber_countrycode) {

		Entity phoneNumber = new Entity("phonenumber", phonenumber);
		phoneNumber.setProperty("phonenumber", phonenumber);
		phoneNumber.setProperty("phonenumber_type", phonenumber_type);
		phoneNumber.setProperty("phonenumber_carrier", phonenumber_carrier);
		phoneNumber.setProperty("phonenumber_countrycode",
				phonenumber_countrycode);
		EntityUtil.persistEntity(phoneNumber);

	}

	public static Iterable<Entity> getAllPhoneNumbers() {
		Iterable<Entity> entities = EntityUtil
				.listEntities("phonenumber", null, null);
		return entities;
	}

	public static Iterable<Entity> getPhoneNumber(String phoneNumber) {
		Iterable<Entity> entities = EntityUtil.listEntities("phonenumber",
				"phonenumber", phoneNumber);
		return entities;
	}

	public static Entity getSinglePhoneNumber(String phoneNumber) {
		Key key = KeyFactory.createKey("phonenumber", phoneNumber);
		return EntityUtil.findEntity(key);
	}
}
