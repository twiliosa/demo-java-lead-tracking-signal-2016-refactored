package com.twilio.signal.demo.entities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterators;
import com.twilio.signal.demo.util.EntityUtil;

public class TwilioLeadTrackingSignalDemoEntityCALLSTATUS {
	
	public static void createOrUpdateCallStates(int initiatedCount, int ringingCount, int inprogressCount,
			int completedCount) {
		Entity callState = getSingleCallState("callstate");
		if (callState == null) {
			callState = new Entity("callstate", "callstate");
		}
		callState.setProperty("initiatedCount", initiatedCount);
		callState.setProperty("ringingCount", ringingCount);
		callState.setProperty("inprogressCount", inprogressCount);
		callState.setProperty("completedCount", completedCount);
		EntityUtil.persistEntity(callState);
	}

	public static Iterable<Entity> getAllCallStates() {
		Iterable<Entity> entities = EntityUtil.listEntities("callstate", null, null);
		return entities;
	}

	public static Iterable<Entity> getCallState(String callState) {
		Iterable<Entity> entities = EntityUtil.listEntities("callstate", "callstate",
				callState);
		return entities;
	}

	public static Entity getSingleCallState(String callState) {
		Key key = KeyFactory.createKey("callstate", callState);
		return EntityUtil.findEntity(key);
	}

	public static int getCallCount() {
		Iterable<Entity> entities = EntityUtil.listEntities("callstate", null, null);
		Iterator<Entity> callStateItr = entities.iterator();
		int size = Iterators.size(callStateItr);

		return size;

	}
	
	public Map<String, Integer> getCallStateStats() {
		
		Map<String, Integer> callStateCount = new HashMap<String, Integer>();
		
		return callStateCount;
	}

}
