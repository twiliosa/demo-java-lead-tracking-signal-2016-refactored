package com.twilio.signal.demo.entities;

import java.util.Iterator;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterators;
import com.twilio.signal.demo.util.EntityUtil;
import com.twilio.signal.demo.util.Helper;

public class TwilioLeadTrackingSignalDemoEntityLEAD {

	public static void createOrUpdatelead(String name, String phnno,
			String selectedOption, String countryCode) {
		Entity lead = getSinglelead(name);
		if (lead == null) {
			lead = new Entity("lead", phnno);
			lead.setProperty("name", name);
			lead.setProperty("leadid", Helper.getUniqueID());
			lead.setProperty("phnno", phnno);
			lead.setProperty("selectedoption", selectedOption);
			lead.setProperty("countrycode", countryCode);
		}
		EntityUtil.persistEntity(lead);
	}

	public static Iterable<Entity> getAllleads() {
		Iterable<Entity> entities = EntityUtil.listEntities("lead", null, null);
		return entities;
	}

	public static Iterable<Entity> getlead(String phnno) {
		Iterable<Entity> entities = EntityUtil.listEntities("lead", "phnno",
				phnno);
		return entities;
	}

	public static Entity getSinglelead(String phnno) {
		Key key = KeyFactory.createKey("lead", phnno);
		return EntityUtil.findEntity(key);
	}

	public static int getLeadCount() {
		Iterable<Entity> entities = EntityUtil.listEntities("lead", null, null);
		Iterator<Entity> leadItr = entities.iterator();
		int size = Iterators.size(leadItr);

		return size;

	}

}
