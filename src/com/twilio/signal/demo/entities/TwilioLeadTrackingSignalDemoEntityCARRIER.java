package com.twilio.signal.demo.entities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterables;
import com.twilio.signal.demo.util.EntityUtil;

public class TwilioLeadTrackingSignalDemoEntityCARRIER {
	
	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingSignalDemoEntityCARRIER.class.getCanonicalName());

	public static void createOrUpdatePhoneNumberCarrier(
			String phonenumber_carrier) {


		
		
		Entity phoneNumberCarrier = getSinglePhoneNumberCarrier(phonenumber_carrier);
		if (phoneNumberCarrier == null) {
			phoneNumberCarrier = new Entity("carrier", phonenumber_carrier);
			phoneNumberCarrier.setProperty("carrier", phonenumber_carrier);
			phoneNumberCarrier.setProperty("carrier_count", 1);
		}
		// we already have this carrier listed. lets increase the count
		else if (phoneNumberCarrier != null) {
			String carrierCount = phoneNumberCarrier.getProperty(
					"carrier_count").toString();
			Integer carrierCountInt = Integer.valueOf(carrierCount);
			carrierCountInt = carrierCountInt + 1;
			phoneNumberCarrier.setProperty("carrier_count", carrierCountInt);
		}
		EntityUtil.persistEntity(phoneNumberCarrier);

	}

	public static Iterable<Entity> getAllCarriers() {
		Iterable<Entity> entities = EntityUtil.listEntities("carrier", null, null);
		return entities;
	}

	public static Iterable<Entity> getPhoneNumberCarrier(
			String phoneNumberCarrier) {
		Iterable<Entity> entities = EntityUtil.listEntities("carrier", "carrier",
				phoneNumberCarrier);
		return entities;
	}

	public static Entity getSinglePhoneNumberCarrier(String carrier) {
		Key key = KeyFactory.createKey("carrier", carrier);
		return EntityUtil.findEntity(key);
	}

	public static Map<String, Integer> getLookupCarrierStats() {

		Iterable<Entity> entities = getAllCarriers();
		Iterator<Entity> phoneNumberItr = entities.iterator();
		Map<String, Integer> carrierCountMap = new HashMap<String, Integer>();

		while (phoneNumberItr.hasNext()) {
			Entity phoneNumberEntity = phoneNumberItr.next();
			String phoneNumber = phoneNumberEntity.getProperty("carrier")
					.toString();
			String phoneNumberCarrier = phoneNumberEntity
					.getProperty("carrier").toString();

			// query datastore to see if we have covered the carrier
			Entity carrierEntity = TwilioLeadTrackingSignalDemoEntityCARRIER
					.getSinglePhoneNumberCarrier(phoneNumberCarrier);
			if (carrierEntity != null) {
				String carrier = carrierEntity.getProperty("carrier")
						.toString();
				String carrierCount = carrierEntity
						.getProperty("carrier_count").toString();
				carrierCountMap.put(carrier, Integer.valueOf(carrierCount));

			}
			logger.log(Level.INFO, "Total Carrier Entities",
					+Iterables.size(entities));

		}
		return carrierCountMap;

	}

}
